#!/bin/bash

# tell where to search for lib
export DYLD_LIBRARY_PATH=$(pwd)/build/interface_lib:$DYLD_LIBRARY_PATH

# compile
g++ ./tests/src/test_run.cpp -L./build/interface_lib -lnrealAirLibrary_mac -o $(pwd)/tests/build/test_run

# run
./tests/build/driver_mac