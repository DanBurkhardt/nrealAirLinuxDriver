#pragma once

#include "Fusion.h"
#include "hidapi.h"
#include "json.h"
#include "crc32.h"
#include "device3.h"
#include "device4.h"

#define AIRAPI_MAC __attribute__((visibility("public")))

#if __cplusplus
extern "C" {
#endif
    // Function to start connection to Air
    int StartConnection();

    // Function to stop connection to Air
    int StopConnection();

    // Function to start device motion monitoring
    int OpenDeviceMotionStream();

    // Function to start device brightness monitoring
    int OpenDeviceBrightnessListener();

    // Function to get brightness
    int GetBrightness();

    // Retrieves calculated orientation as roll, pitch, and yaw
    float* GetEuler();

    // Function to get quaternion
    float* GetQuaternion();

    // TODO: re-implement these
    // Function to restart a connection to Air
    // int RestartConnection(void);
    // Function to set a new value for Air
    // telemetry sample rate per second (in hz)
    // int UpdateSampleRate(const unsigned int newSampleRate);
#if __cplusplus
}   // Extern C
#endif
