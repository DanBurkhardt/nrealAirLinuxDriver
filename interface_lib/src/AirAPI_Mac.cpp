#include "AirAPI_Mac.h"

#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>

std::atomic<bool> keepRunning(true);

std::mutex dev4_mtx;
std::mutex dev3_mtx;

std::thread brightness_thread;
std::thread devicemotion_thread;

// device attrs
int currentBrightness = 0;

// position & orientation
std::atomic<float*> euler_angles(new float[3]{0.0, 0.0, 0.0});
float* quaternion_orientation = new float[4];

// logging flags
bool verboseOrientationLogging = false;
bool verboseBrightnessLogging = false;

void parseDeviceBrightness(uint64_t timestamp, device4_event_type event, uint8_t brightness, const char* msg) {
    // update public var
    std::lock_guard<std::mutex> lock(dev4_mtx);
    currentBrightness = brightness;

    // log state update
    if(verboseBrightnessLogging == true) {
        switch (event) {
        case DEVICE4_EVENT_MESSAGE:
            printf("Message: `%s`\n", msg);
            break;
        case DEVICE4_EVENT_BRIGHTNESS_UP:
            printf("Increase Brightness: %u\n", brightness);
            break;
        case DEVICE4_EVENT_BRIGHTNESS_DOWN:
            printf("Decrease Brightness: %u\n", brightness);
            break;
        default:
            break;
        }
    }
}


void parseMotionUpdate(uint64_t timestamp,
		   device3_event_type event,
		   const device3_ahrs_type* ahrs) {

	static device3_quat_type old;
	static float dmax = -1.0f;
	
	if (event != DEVICE3_EVENT_UPDATE) {
		return;
	}
	
	device3_quat_type q = device3_get_orientation(ahrs);
	
	const float dx = (old.x - q.x) * (old.x - q.x);
	const float dy = (old.y - q.y) * (old.y - q.y);
	const float dz = (old.z - q.z) * (old.z - q.z);
	const float dw = (old.w - q.w) * (old.w - q.w);
	
	const float d = sqrtf(dx*dx + dy*dy + dz*dz + dw*dw);
	
	if (dmax < 0.0f) {
		dmax = 0.0f;
	} else {
		dmax = (d > dmax? d : dmax);
	}
	
	device3_vec3_type e = device3_get_euler(q);
	
	if (d >= 0.00005f) {
		device3_vec3_type e0 = device3_get_euler(old);
		
		if(verboseOrientationLogging == true) {
			printf("Pitch: %f; Roll: %f; Yaw: %f\n", e0.x, e0.y, e0.z);
			printf("Pitch: %f; Roll: %f; Yaw: %f\n", e.x, e.y, e.z);
			printf("D = %f; ( %f )\n", d, dmax);
			
			printf("X: %f; Y: %f; Z: %f; W: %f;\n", old.x, old.y, old.z, old.w);
			printf("X: %f; Y: %f; Z: %f; W: %f;\n", q.x, q.y, q.z, q.w);
		}

	} else {
		if(verboseOrientationLogging == true) {
			printf("Pitch: %.2f; Roll: %.2f; Yaw: %.2f\n", e.x, e.y, e.z);
		}
	}
   
    {
        // transpose all euler
        euler_angles.load()[0] = e.x;
        euler_angles.load()[1] = e.y;
        euler_angles.load()[2] = e.z;
    }

    {
        // transpose all quaternion
        std::unique_lock<std::mutex> lock(dev3_mtx);
        quaternion_orientation[0] = q.w;
        quaternion_orientation[1] = q.x;
        quaternion_orientation[2] = q.y;
        quaternion_orientation[3] = q.z;
        lock.unlock();
    }
    old = q;
}

int OpenDeviceBrightnessListener() {
    device4_type dev4;
    
    if (DEVICE4_ERROR_NO_ERROR != device4_open(&dev4, parseDeviceBrightness)) {
        return 1;
    }

    device4_clear(&dev4);
    while (DEVICE4_ERROR_NO_ERROR == device4_read(&dev4, 0) && keepRunning);

    device4_close(&dev4);
            
    return 0;
}

int StartBrightness() {
    std::cout << "starting device brightness listener.." << std::endl;
    brightness_thread = std::thread(OpenDeviceBrightnessListener);
    return 0;
}

int OpenDeviceMotionStream() {
    
    device3_type dev3;

    if (DEVICE3_ERROR_NO_ERROR != device3_open(&dev3, parseMotionUpdate)) {
        return 1;
    }

    device3_clear(&dev3);
    while(keepRunning) {
        // get err type
        device3_error_type errRes = device3_read(&dev3, 0);
        

        // ignore these error types, but 
        // not others
        // 
        // TODO: the wrong signature error is thrown 
        // every time but device readings are successful anyway. debug.
        if( errRes != DEVICE3_ERROR_NO_ERROR && errRes != DEVICE3_ERROR_WRONG_SIGNATURE ) {
            std::cout << "error reading device 3" << std::endl;
            break;
        } else {
            // std::cout << "device 3 read success" << std::endl;
        }
    }
    device3_close(&dev3);
    return 0;
}

int StartMotion() {
    std::cout << "starting device motion listener..." << std::endl;
    devicemotion_thread = std::thread(OpenDeviceMotionStream);
    return 0;
}

int StartConnection() {
    std::cout << "starting driver... " << std::endl;
    // set logging flag
    verboseOrientationLogging = false;
    verboseBrightnessLogging = false;

    // flag to keep running
    keepRunning.store(true);

    // create and start threads
    StartBrightness();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    StartMotion();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    return 0;
}

int StopConnection() {
    printf("stopping driver... ");
    // set flag to stop running thread ops
    keepRunning.store(false);
    
    if (brightness_thread.joinable()) {
        brightness_thread.join();
    }

    if (devicemotion_thread.joinable()) {
        devicemotion_thread.join();
    }

    // cleanup
    float* eulerArray = euler_angles.exchange(nullptr);
    if(eulerArray) {
        delete[] eulerArray;
    }

    delete[] quaternion_orientation;
    quaternion_orientation = nullptr;

    return 0;
}

int GetBrightness() {
    // safely return
    std::lock_guard<std::mutex> lock(dev4_mtx);
    return currentBrightness;
}

float* GetEuler() {
    {
        return euler_angles.load();
    }
}

float* GetQuaternion() {
    {
        std::lock_guard<std::mutex> lock(dev3_mtx);
        return quaternion_orientation;
    }
}