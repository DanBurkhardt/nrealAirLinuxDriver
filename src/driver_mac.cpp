#ifdef EXAMPLE_TYPE
#include "AirAPI_Mac.h"
#else
#include "nrealAirLibrary.h"
#endif

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
#include <chrono>
#include <thread>

int iterations = 50;

void readEuler() {

	// euler angle reading
	float* euler = GetEuler();

	std::cout << "\nreading euler angles..\n";
	for (size_t i = 0; i <= 2; i++) {
		std::cout<< "euler value at index " << i << ":  " << static_cast<float>(euler[i]) << "\n";
	}
	std::cout << std::endl;
}

void readQuaternion() {

	// quaternion orientation
	float* orientation = GetQuaternion();

	int totalValue = 0;

	std::cout << "\nreading quaternion values.. \n";
	for (size_t i = 0; i <= 3; i++) {
		std::cout<< "quaternion value at index " << i << ":  " << static_cast<float>(orientation[i]) << "\n";
	}
	std::cout << std::endl;
}

void readBrightness() {
	std::cout << "Brightness: " << GetBrightness() << std::endl;
}


int startMotionReader() {
	// Main loop that could represent the rest of your application.
	for (int i = 0; i < iterations; ++i) {
		std::cout << "\nread number: " << i << std::endl;
		// q
		readQuaternion();

		// e
		readEuler();

		// ~60 updates per second
		std::this_thread::sleep_for(std::chrono::milliseconds(16));
	}
	
	return 0;
}


int startBrightnessReader() {
	// Main loop that could represent the rest of your application.
	for (int i = 0; i < iterations; ++i) {
		std::cout << "\nread number: " << i << std::endl;

		// brightness
		readBrightness();
		
		// ~60 updates per second
		std::this_thread::sleep_for(std::chrono::milliseconds(16));
	}
	
	return 0;
}

int start() {
	return StartConnection();
}

int main() {

	std::cout << "\n\nstarting from main" << std::endl;
	start();
	std::cout << "\n\nstart returned" << std::endl;

	std::cout << "\n\nrunning readers" << std::endl;
	startBrightnessReader();
	startMotionReader();

	std::cout << "\n\nattempting to stop the driver" << std::endl;
	std::this_thread::sleep_for(std::chrono::milliseconds(300));
	return StopConnection();
}